# Cipher Site

Проект Cipher - вебсайт со статьями по различным методам кодирования, шифрования, калькуляторами к ним, а также рабочим форумом для общения с единомышленниками.

## Установка

```bash
git clone https://gitlab.com/secretmaxim345/cipher-site/
pip install -r requirements.txt
```

## Документация

Подробнее о данном проекте можно прочитать по следующей [ссылке](https://secretmaxim345.gitlab.io/-/cipher-site/-/jobs/1303730658/artifacts/public/docs/index.html).

Или же:
```bash
cd docs
make html
```
