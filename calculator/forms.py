from django import forms


class EmptyForm(forms.Form):
    pass


class ASCII(forms.Form):
    """
    form that is being used for ascii encryption and decryption

    :param text: text-field that is being encrypted or decrypted
    :param operation: choice field that tells us if the user wants to encrypt or decrypt a text
    """
    text = forms.CharField(label='Текст:', widget=forms.Textarea)
    operation = forms.ChoiceField(label='', choices=(("encrypt", "Закодировать"),
                                                     ("decrypt", "Декодировать")))


class Base64(forms.Form):
    """
    form that is being used for base64 encryption and decryption

    :param text: text-field that is being encrypted or decrypted
    :param operation: choice field that tells us if the user wants to encrypt or decrypt a text
    """
    text = forms.CharField(label='Текст:', widget=forms.Textarea)
    operation = forms.ChoiceField(label='', choices=(("encrypt", "Закодировать"),
                                                     ("decrypt", "Декодировать")))


class Caesar(forms.Form):
    """
    form that is being used for caesar encryption and decryption

    :param text: text-field that is being encrypted or decrypted
    :param shift: the shift in which the text will be encrypted or in which the text was encrypted
    :param operation: choice field that tells us if the user wants to encrypt or decrypt a text
    """
    text = forms.CharField(label='Текст:')
    shift = forms.IntegerField(label='Сдвиг:')
    operation = forms.ChoiceField(label='', choices=(("encrypt", "Сдвиг вперёд"),
                                                     ("decrypt", "Сдвиг назад")))


class Vigenere(forms.Form):
    """
    form that is being used for vigenere encryption and decryption

    :param text: text-field that is being encrypted or decrypted
    :param key: the key in which the text will be encrypted or in which the text was encrypted
    :param operation: choice field that tells us if the user wants to encrypt or decrypt a text
    """
    text = forms.CharField(label='Текст:')
    key = forms.CharField(label='Ключ:')
    operation = forms.ChoiceField(label='', choices=(("encrypt", "Сдвиг вперёд"),
                                                     ("decrypt", "Сдвиг назад")))


class Xor(forms.Form):
    """
    form that is being used for vigenere encryption and decryption

    :param text: text-field that is being encrypted or decrypted
    :param key: the key in which the text will be encrypted or in which the text was encrypted
    """
    text = forms.CharField(label='Строка:')
    key = forms.CharField(label='Ключ:')


class DiffieHellman(forms.Form):
    """
    form that is being used for vigenere encryption and decryption
    """
    base = forms.IntegerField(label='P:')
    modulus = forms.IntegerField(label='Q:')
    secret_a = forms.IntegerField(label='A:')
    secret_b = forms.IntegerField(label='B:')


class Hash(forms.Form):
    """
    form that is being used to make a hash

    :param text: text-field that will be turned into a hash
    """
    text = forms.CharField(label='')
