from django.test import TestCase, Client
from django.urls import reverse


class AsciiCalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_ascii_response(self):
        self.response = self.client.get(reverse('ascii'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/ascii_calculator.html')

    def test_ascii_result(self):
        data = {'text': 'axaxaxaxaax', 'operation': 'encrypt'}
        response = self.client.post(reverse('ascii'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/ascii_calculator.html')
        self.assertEqual(response.context['result'], '97 120 97 120 97 120 97 120 97 97 120 ')

    def test_ascii_result_2(self):
        data = {'text': '97 120 97 120 97 120 97 120 97 97 120 ', 'operation': 'decrypt'}
        response = self.client.post(reverse('ascii'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/ascii_calculator.html')
        self.assertEqual(response.context['result'], 'axaxaxaxaax')

    def test_ascii_result_3(self):
        data = {'text': 'Привет, мир!', 'operation': 'encrypt'}
        response = self.client.post(reverse('ascii'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/ascii_calculator.html')
        self.assertEqual(response.context['result'], '207 240 232 226 229 242 44 32 236 232 240 33 ')

    def test_ascii_result_4(self):
        data = {'text': '207 240 232 226 229 242 44 32 236 232 240 33', 'operation': 'decrypt'}
        response = self.client.post(reverse('ascii'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/ascii_calculator.html')
        self.assertEqual(response.context['result'], 'Привет, мир!')


class Base64CalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_base64_response(self):
        self.response = self.client.get(reverse('base64'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/base64.html')

    def test_base64_result(self):
        data = {'text': 'Привет, мир!', 'operation': 'encrypt'}
        response = self.client.post(reverse('base64'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/base64.html')
        self.assertEqual(response.context['result'], '0J/RgNC40LLQtdGCLCDQvNC40YAh')

    def test_base64_result_2(self):
        data = {'text': '0J/RgNC40LLQtdGCLCDQvNC40YAh', 'operation': 'decrypt'}
        response = self.client.post(reverse('base64'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/base64.html')
        self.assertEqual(response.context['result'], 'Привет, мир!')

    def test_base64_result_3(self):
        data = {'text': 'Hey? Are you here?', 'operation': 'encrypt'}
        response = self.client.post(reverse('base64'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/base64.html')
        self.assertEqual(response.context['result'], 'SGV5PyBBcmUgeW91IGhlcmU/')

    def test_base64_result_4(self):
        data = {'text': 'SGV5PyBBcmUgeW91IGhlcmU/', 'operation': 'decrypt'}
        response = self.client.post(reverse('base64'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/base64.html')
        self.assertEqual(response.context['result'], 'Hey? Are you here?')


class CaesarCalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_caesar_response(self):
        self.response = self.client.get(reverse('caesar'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/caesar.html')

    def test_caesar_result(self):
        data = {'text': 'Victory!', 'shift': '0', 'operation': 'encrypt'}
        response = self.client.post(reverse('caesar'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/caesar.html')
        self.assertEqual(response.context['result'], 'Victory!')

    def test_caesar_result_2(self):
        data = {'text': 'Victory!', 'shift': '0', 'operation': 'decrypt'}
        response = self.client.post(reverse('caesar'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/caesar.html')
        self.assertEqual(response.context['result'], 'Victory!')

    def test_caesar_result_3(self):
        data = {'text': 'Just a text...', 'shift': '16', 'operation': 'encrypt'}
        response = self.client.post(reverse('caesar'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/caesar.html')
        self.assertEqual(response.context['result'], 'Zkij q junj...')

    def test_caesar_result_4(self):
        data = {'text': 'Zkij q junj...', 'shift': '16', 'operation': 'decrypt'}
        response = self.client.post(reverse('caesar'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/caesar.html')
        self.assertEqual(response.context['result'], 'Just a text...')


class DiffieHellmanCalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_diffieHellman_response(self):
        self.response = self.client.get(reverse('diffiehellman'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/diffieHellman.html')

    def test_diffieHellman_result(self):
        data = {'modulus': '541', 'base': '10', 'secret_a': '12', 'secret_b': '43'}
        response = self.client.post(reverse('diffiehellman'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/diffieHellman.html')
        self.assertEqual(response.context['result'], 237)

    def test_diffieHellman_result_2(self):
        data = {'modulus': '407', 'base': '9', 'secret_a': '305', 'secret_b': '21'}
        response = self.client.post(reverse('diffiehellman'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/diffieHellman.html')
        self.assertEqual(response.context['result'], 232)

    def test_diffieHellman_result_3(self):
        data = {'modulus': '576', 'base': '101', 'secret_a': '25', 'secret_b': '690'}
        response = self.client.post(reverse('diffiehellman'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/diffieHellman.html')
        self.assertEqual(response.context['result'], 217)

    def test_diffieHellman_result_4(self):
        data = {'modulus': '25', 'base': '97', 'secret_a': '654', 'secret_b': '23'}
        response = self.client.post(reverse('diffiehellman'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/diffieHellman.html')
        self.assertEqual(response.context['result'], 9)


class VigenereCalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_vigenere_response(self):
        self.response = self.client.get(reverse('vigenere'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/vigenere.html')

    def test_vigenere_result(self):
        data = {'text': 'hhaha', 'key': 'English', 'operation': 'encrypt'}
        response = self.client.post(reverse('vigenere'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/vigenere.html')
        self.assertEqual(response.context['result'], 'lugsi')

    def test_vigenere_result_2(self):
        data = {'text': 'xrye uwzwnmp', 'key': 'English', 'operation': 'decrypt'}
        response = self.client.post(reverse('vigenere'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/vigenere.html')
        self.assertEqual(response.context['result'], 'test message')

    def test_vigenere_result_3(self):
        data = {'text': 'test message', 'key': 'English', 'operation': 'encrypt'}
        response = self.client.post(reverse('vigenere'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/vigenere.html')
        self.assertEqual(response.context['result'], 'xrye uwzwnmp')

    def test_vigenere_result_4(self):
        data = {'text': 'test message', 'key': 'English', 'operation': 'decrypt'}
        response = self.client.post(reverse('vigenere'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/vigenere.html')
        self.assertEqual(response.context['result'], 'prmi emlonat')


class XorCalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_xor_response(self):
        self.response = self.client.get(reverse('xor'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/xor.html')

    def test_xor_result(self):
        data = {'text': 'FW_XVq[RbU@cUSAu][Qd', 'key': '12345A67B89C123D456E'}
        response = self.client.post(reverse('xor'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/xor.html')
        self.assertEqual(response.context['result'], 'wellc0me my dar1ing!')

    def test_xor_result_2(self):
        data = {'text': '* y!68y:y,8!y=j?j.-o).*<', 'key': 'YOYOYOYOYOYOYOYOYOYOYOYO'}
        response = self.client.post(reverse('xor'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/xor.html')
        self.assertEqual(response.context['result'], 'so now u can r3p3at pass')

    def test_xor_result_3(self):
        data = {'text': '?) /"<q=t6qe;7 y7)8i$', 'key': 'YEAHYEAHYEAHYEAHYEAHY'}
        response = self.client.post(reverse('xor'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/xor.html')
        self.assertEqual(response.context['result'], 'flag{y0u-s0-bra1nly!}')

    def test_xor_result_4(self):
        data = {'text': '2#52f64v/"u(f+v*v"#%', 'key': 'FFFFFFFFFFFFFFFFFFFF'}
        response = self.client.post(reverse('xor'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/xor.html')
        self.assertEqual(response.context['result'], 'test pr0id3n m0l0dec')


class MD5CalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_md5_response(self):
        self.response = self.client.get(reverse('md5'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/hash/md5.html')

    def test_md5_result(self):
        data = {'text': 'print(\'Hahahaha\')'}
        response = self.client.post(reverse('md5'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/md5.html')
        self.assertEqual(response.context['result'], '1507cf0be62bbeb83464b7e7fe50489c')

    def test_md5_result_2(self):
        data = {'text': 'helloworld'}
        response = self.client.post(reverse('md5'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/md5.html')
        self.assertEqual(response.context['result'], 'fc5e038d38a57032085441e7fe7010b0')

    def test_md5_result_3(self):
        data = {'text': 'zddkhvkjk'}
        response = self.client.post(reverse('md5'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/md5.html')
        self.assertEqual(response.context['result'], 'd2ff0ad4ac6963eaf258a00dc844453d')

    def test_md5_result_4(self):
        data = {'text': 'password'}
        response = self.client.post(reverse('md5'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/md5.html')
        self.assertEqual(response.context['result'], '5f4dcc3b5aa765d61d8327deb882cf99')


class SHA1CalculatorTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_sha1_response(self):
        self.response = self.client.get(reverse('sha1'))
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'calculators/hash/sha1.html')

    def test_sha1_result(self):
        data = {'text': 'helloworld'}
        response = self.client.post(reverse('sha1'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/sha1.html')
        self.assertEqual(response.context['result'], '6adfb183a4a2c94a2f92dab5ade762a47889a5a1')

    def test_sha1_result_2(self):
        data = {'text': 'ыфово'}
        response = self.client.post(reverse('sha1'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/sha1.html')
        self.assertEqual(response.context['result'], 'cd3fc23d85d0b89468797844f7906a28011808c3')

    def test_sha1_result_3(self):
        data = {'text': 'print(\'Hahahaha\')'}
        response = self.client.post(reverse('sha1'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/sha1.html')
        self.assertEqual(response.context['result'], 'c9ada49981065a98bc086c097c08140930d061f0')

    def test_sha1_result_4(self):
        data = {'text': 'qdeer'}
        response = self.client.post(reverse('sha1'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'calculators/hash/sha1.html')
        self.assertEqual(response.context['result'], 'ee6d0ccfd49ef8bb2668a70e509e73310fd8a3c4')
