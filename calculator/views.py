import base64
import itertools
import hashlib
from abc import abstractmethod

from django.shortcuts import render
from django.views import View
import calculator.forms as forms


class OneChoiceCalc(View):
    """
    This is a base class for all calculators that can only encrypt

    :param form: a form that is going to be used
    :type form: :class:`django.forms.form`
    :param page_path: path to the html file
    :type page_path: str
    :param page_name: title of the html page
    :type page_name: str
    :param page_heading: heading of the html page
    :type page_heading: str
    """
    form = forms.EmptyForm

    page_path = ''
    page_name = ''

    def get(self, request):
        """
        'get' method of this class.

        This method sends two forms(`encryption_form`, `decryption_form`)
        to the client in html format.

        :param request: an object with request details
        :type request: :class:`django.http.HttpRequest`
        :return: server response object with HTML code
        :rtype: :class:`django.http.HttpResponse`
        """
        new_form = self.form()

        context = {
            'form': new_form,
            'page_name': self.page_name,
        }

        return render(request, self.page_path, context)

    def post(self, request):
        """
        'post' method of this class.

        This method receives two forms and sends the encryption and decryption result
        to the client in html format.

        :param request: an object with request details
        :type request: :class:`django.http.HttpRequest`
        :return: server response object with HTML code
        :rtype: :class:`django.http.HttpResponse`
        """
        form = self.form(request.POST)

        result = ''
        if form.is_valid():
            result = self.calculate_result(form)

        context = {
            'form': form,
            'result': result,
            'page_name': self.page_name,
        }

        return render(request, self.page_path, context)

    @abstractmethod
    def calculate_result(self, form):
        """
        It is an abstract method that calculates the result.

        :param form: form with data for encryption
        :type form: :class:`django.forms.form`
        :return: result
        :rtype: str
        """
        return 'None'


class TwoChoiceCalc(OneChoiceCalc):
    """
    This is a base class for all calculators that can encrypt and decrypt
    """

    def calculate_result(self, form):
        """
        It is method that decides if the data needs to be encrypted or decrypted

        :param form: form with data
        :type form: :class:`django.forms.form`
        :return: result
        :rtype: str
        """

        result = ''

        if form.cleaned_data["operation"] == "encrypt":
            result = self.encrypt(form)
        if form.cleaned_data["operation"] == "decrypt":
            result = self.decrypt(form)

        return result

    @abstractmethod
    def encrypt(self, form):
        """
        It is an abstract method that calculated the encryption result.

        :param form: form with data for encryption
        :type form: :class:`django.forms.form`
        :return: encryption result
        :rtype: str
        """
        return 'None'

    @abstractmethod
    def decrypt(self, form):
        """
        It is an abstract method that calculated the decryption result.

        :param form: form with data for decryption
        :type form: :class:`django.forms.form`
        :return: decryption result
        :rtype: str
        """
        return 'None'


class ASCIICalc(TwoChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.TwoChoiceCalc`.
    The purpose of this class, is the encryption and decryption of ASCII.
    """
    form = forms.ASCII

    page_path = 'calculators/ascii_calculator.html'
    page_name = 'ASCII'

    def encrypt(self, form):
        """
        This method is encrypting a text into ASCII

        :param form: data that is needed for ASCII encryption
        :type form: :class:`django.forms.form`
        :return: encryption result
        :rtype: str
        """
        encrypt_data = form.cleaned_data['text']
        encrypt_result = ''
        for character in encrypt_data:
            coded_char = ord(character)
            if coded_char > 1040:
                coded_char -= 848

            encrypt_result += str(coded_char) + " "

        return encrypt_result

    def decrypt(self, form):
        """
        This method is decrypting a text out of ASCII

        :param form: data that is needed for ASCII decryption
        :type form: :class:`django.forms.form`
        :return: decryption result
        :rtype: str
        """
        try:
            decrypt_data = form.cleaned_data['text']
            decrypt_result = ''
            ascii_codes = decrypt_data.split()
            for code in ascii_codes:
                coded_char = int(code)
                if coded_char >= 192:
                    coded_char += 848
                decrypt_result += chr(coded_char)
        except ValueError:
            decrypt_result = 'введённые данные некорректны'

        return decrypt_result


class Base64Calc(TwoChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.TwoChoiceCalc`.
    The purpose of this class, is the encryption and decryption of Base64.
    """

    form = forms.Base64

    page_path = 'calculators/base64.html'
    page_name = 'BASE64'

    def encrypt(self, form):
        """
        This method is encrypting a text into base64

        :param form: data that is needed for base64 encryption
        :type form: :class:`django.forms.form`
        :return: encryption result
        :rtype: str
        """
        encrypt_result = base64.b64encode(bytes(form.cleaned_data['text'], 'utf-8')).decode('utf-8')
        return encrypt_result

    def decrypt(self, form):
        """
        This method is decrypting a text out of base64

        :param form: data that is needed for base64 decryption
        :type form: :class:`django.forms.form`
        :return: decryption result
        :rtype: str
        """
        try:
            decrypt_result = base64.b64decode(form.cleaned_data['text'].encode('ascii')).decode('utf-8')
        except Exception:
            decrypt_result = 'введённые данные некорректны'

        return decrypt_result


class CaesarCalc(TwoChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.TwoChoiceCalc`.
    The purpose of this class, is the encryption and decryption of Caesar cipher.
    """
    form = forms.Caesar

    page_path = 'calculators/caesar.html'
    page_name = 'Шифр Цезаря'

    def encrypt(self, form):
        """
        This method is encrypting a text into caesar cipher

        :param form: data that is needed for caesar cipher encryption
        :type form: :class:`django.forms.form`
        :return: encryption result
        :rtype: str
        """
        shift = form.cleaned_data['shift']
        return self.caesar(form.cleaned_data['text'], int(shift))

    def decrypt(self, form):
        """
        This method is decrypting a text out of caesar cipher

        :param form: data that is needed for caesar cipher decryption
        :type form: :class:`django.forms.form`
        :return: decryption result
        :rtype: str
        """
        shift = form.cleaned_data['shift']
        return self.caesar(form.cleaned_data['text'], -int(shift))

    @staticmethod
    def caesar(text, shift):
        """
        This method is shifting the text alphabetically (caesar cipher)

        :param text: text that is being shifted
        :type text: str
        :param shift: shift that is shifting the text
        :type shift: int
        :return: shifted text
        :rtype: str
        """
        result = ''
        for character in text:
            lowercase = False
            if ('A' <= character <= 'Z') or ('a' <= character <= 'z'):
                if character.islower():
                    lowercase = True
                    character = character.upper()

                new_ch = ord(character) + (shift % 26)

                if new_ch > ord('Z'):
                    new_ch = chr(ord('A') + ((new_ch - ord('Z')) % 26) - 1)
                elif new_ch < ord('A'):
                    new_ch = chr(ord('Z') - ((ord('A') - new_ch) % 26) + 1)
                else:
                    new_ch = chr(new_ch)

                if lowercase:
                    new_ch = new_ch.lower()

                result += new_ch

            elif ('А' <= character <= 'Я') or ('а' <= character <= 'я'):
                if character.islower():
                    lowercase = True
                    character = character.upper()

                new_ch = ord(character) + (shift % 32)

                if new_ch > ord('Я'):
                    new_ch = chr(ord('А') + ((new_ch - ord('Я')) % 32) - 1)
                elif new_ch < ord('А'):
                    new_ch = chr(ord('Я') - ((ord('А') - new_ch) % 32) + 1)
                else:
                    new_ch = chr(new_ch)

                if lowercase:
                    new_ch = new_ch.lower()

                result += new_ch

            else:
                result += character

        return result


class VigenereCalc(TwoChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.TwoChoiceCalc`.
    The purpose of this class, is the encryption and decryption of vigenere cipher.
    """
    page_name = 'Шифр Виженера'
    page_path = 'calculators/vigenere.html'

    form = forms.Vigenere

    def encrypt(self, form):
        """
        This method is encrypting a text into vigenere cipher

        :param form: data that is needed for vigenere cipher encryption
        :type form: :class:`django.forms.form`
        :return: encryption result
        :rtype: str
        """
        text, key = form.cleaned_data['text'], form.cleaned_data['key']

        for character in text:
            if not (character.isalpha() or character == ' '):
                return 'введённые данные некорректны'
        for character in key:
            if not (character.isalpha() or character == ' '):
                return 'введённые данные некорректны'

        result = ''
        special_symbols = 0
        for i in range(0, len(text)):
            character = text[i]

            lowercase = False
            if character.islower():
                lowercase = True
                character = character.upper()

            if 'A' <= character <= 'Z':
                new_ch = chr(
                    ord('A') + ((self.char_value(text[i]) +
                                 self.char_value(key[(i - special_symbols) % len(key)])) % 26))
                if lowercase:
                    new_ch = new_ch.lower()
                result += new_ch

            else:
                special_symbols += 1
                result += text[i]

        return result

    def decrypt(self, form):
        """
        This method is decrypting a text out of vigenere cipher

        :param form: data that is needed for vigenere cipher decryption
        :type form: :class:`django.forms.form`
        :return: decryption result
        :rtype: str
        """
        text, key = form.cleaned_data['text'], form.cleaned_data['key']

        for character in text:
            if not (character.isalpha() or character == ' '):
                return 'введённые данные некорректны'
        for character in key:
            if not (character.isalpha() or character == ' '):
                return 'введённые данные некорректны'

        result = ''
        special_symbols = 0
        for i in range(0, len(text)):
            character = text[i]

            lowercase = False
            if character.islower():
                lowercase = True
                character = character.upper()

            if 'A' <= character <= 'Z':
                new_ch = chr(
                    ((26 + (self.char_value(text[i]) - self.char_value(
                        key[(i - special_symbols) % len(key)]))) % 26) + ord('A'))
                if lowercase:
                    new_ch = new_ch.lower()
                result += new_ch

            else:
                special_symbols += 1
                result += text[i]

        return result

    @staticmethod
    def char_value(character):
        """
        this method gets the numerical place of the letter in alphabet

        :param character: the letter which place we want to find out
        :type character: chr
        :return: the place where the letter is in the alphabet
        :rtype: int
        """
        return ord(character.upper()) - ord('A')


class XorCalc(OneChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.OneChoiceCalc`.
    The purpose of this class, is the encryption and decryption a text using xor.
    """
    form = forms.Xor

    page_path = 'calculators/xor.html'
    page_name = 'XOR'

    def calculate_result(self, form):
        """
        This method is xoring the text

        :param form: form that contains data for xoring
        :type form: :class:`django.forms.form`
        :return: result
        :rtype: str
        """
        message = form.cleaned_data['text']
        key = form.data['key']

        return ''.join(chr(ord(c) ^ ord(k)) for c, k in zip(message, itertools.cycle(key)))


class DiffieHellman(OneChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.OneChoiceCalc`.
    The purpose of this class, is to get a secret number using diffiehellman protocol.
    """
    form = forms.DiffieHellman

    page_path = 'calculators/diffieHellman.html'
    page_name = 'Протокол Диффи-Хеллмана'

    def calculate_result(self, form):
        """
        This method is calculating the secret number using diffiehellman protocol

        :param form: form that contains data for diffiehellman protocol
        :type form: :class:`django.forms.form`
        :return: secret number
        :rtype: str
        """
        print(type(form))
        base = int(form.cleaned_data['base'])
        modulus = int(form.cleaned_data['modulus'])
        secret_a = int(form.cleaned_data['secret_a'])
        secret_b = int(form.cleaned_data['secret_b'])

        if base <= 0 or modulus <= 0 or secret_a <= 0 or secret_b <= 0:
            return 'введённые данные некорректны'

        a_power = base ** secret_a % modulus
        mutual_secret = a_power ** secret_b % modulus

        return mutual_secret


class MD5(OneChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.OneChoiceCalc`.
    The purpose of this class, is to get a md5 hash of a text.
    """
    form = forms.Hash

    page_path = 'calculators/hash/md5.html'
    page_name = 'MD5'

    def calculate_result(self, form):
        """
        This method is turning a text into a md5 hash

        :param form: form that contains text that is going to be hashed
        :type form: :class:`django.forms.form`
        :return: hash
        :rtype: str
        """
        data = form.cleaned_data['text']
        result = hashlib.md5(bytes(data, 'utf-8')).hexdigest()
        return result


class SHA1(OneChoiceCalc):
    """
    This is a derived class which is inherited from :class:`calculators.views.OneChoiceCalc`.
    The purpose of this class, is to get a sha1 hash of a text.
    """
    form = forms.Hash

    page_path = 'calculators/hash/sha1.html'
    page_name = 'SHA1'

    def calculate_result(self, form):
        """
        This method is turning a text into a sha1 hash

        :param form: form that contains text that is going to be hashed
        :type form: :class:`django.forms.form`
        :return: hash
        :rtype: str
        """
        data = form.cleaned_data['text']
        result = hashlib.sha1(bytes(data, 'utf-8')).hexdigest()
        return result
