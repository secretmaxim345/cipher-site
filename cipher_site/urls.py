"""cipher_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from main import views as main_views
from users import views as user_views
from calculator import views as calc_views
from forum import views as forum_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),
    path('', main_views.home, name='home'),
    path('signup/', user_views.Registration.as_view(), name='register'),
    path('signin/', auth_views.LoginView.as_view(
        extra_context={
                'page_name': 'Войти',
        }
    ), name='login'),
    path('signout/', auth_views.LogoutView.as_view(), name='logout'),
    path('edit_profile/', user_views.EditProfile.as_view(), name='edit_profile'),
    path('lessons/', main_views.lessons, name='lessons'),

    path('ascii/', calc_views.ASCIICalc.as_view(), name='ascii'),
    path('base64/', calc_views.Base64Calc.as_view(), name='base64'),
    path('caesar/', calc_views.CaesarCalc.as_view(), name='caesar'),
    path('vigenere/', calc_views.VigenereCalc.as_view(), name='vigenere'),
    path('xor/', calc_views.XorCalc.as_view(), name='xor'),
    path('diffiehellman/', calc_views.DiffieHellman.as_view(), name='diffiehellman'),

    path('md5/', calc_views.MD5.as_view(), name='md5'),
    path('sha1/', calc_views.SHA1.as_view(), name='sha1'),

    path('forum/', forum_views.ListThreads.as_view(), name='forum'),
    path('forum/create', forum_views.create_thread, name='thread_create'),
    path('forum/thread/', include('forum.urls'), name='thread'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
