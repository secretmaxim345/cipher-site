#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='ckeditor',
      version='6.0.0',
      packages=find_packages())