Calculator documentation
========================

*****
forms
*****

.. automodule:: calculator.forms
    :members:

*****
views
*****

.. automodule:: calculator.views
    :members: