How to use the calculators?
===========================

******
Step 1
******

Welcome to this guide. In this guide I am going to show you how to use calculators
on our website. It's pretty easy to do. Just follow my steps.
To create a new account, it isn't important on which page you are. On the top
right corner you can see a blue button that says "Войти". If you see a blue button
saying "Выйти" instead it means you are already logged in. Click on the blue button
saying "Войти". It will redirect you to a login page.

..  image:: ../pictures/calcUsage-step1.png

******
Step 2
******

Now you will see a list of lessons. Choose one of them. To go to the lesson just
click the "Читать далее" button. For the purpose of this guide i am going to
choose the ascii lesson.

..  image:: ../pictures/calcUsage-step2.png

******
Step 3
******

Now you are on the lesson page. If your scroll down you will find a calculator.
To encrypt/decrypt you will need to fill out the fields. If you don't know what
to write into the field just read the text above. It will explain you how the
cipher works and you will know what each field does!

..  image:: ../pictures/calcUsage-step3.png


Congratulations! Now you know how to use the calculators! You are one step closer
of becoming an experienced user!
