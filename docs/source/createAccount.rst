How to create a new account?
============================

******
Step 1
******

Welcome to this guide. In this guide I am going to show you how to create a new
account on our website. It's pretty easy to do. Just follow my steps.
To create a new account, it isn't important on which page you are. On the top
right corner you can see a blue button that says "Войти". If you see a blue button
saying "Выйти" instead it means you are already logged in. Click on the blue button
saying "Войти". It will redirect you to a login page.

..  image:: ../pictures/createAccount-step1.png

******
Step 2
******

On this page the website will offer you to log in, but we need to create a new
account, so we click on the blue link down below. It will redirect you to the
registration page.

..  image:: ../pictures/createAccount-step2.png

******
Step 3
******

Now when you are on the registration page you will see a registration form that you
need to fill out to create a new account. In the first field you need to enter your
new account name. The account name will be visible to all other users on the site.
In the next field you need to enter your email. It is very important that the email
is valid! In the third field you need to enter your first name and in the fourth
field you need to enter your last name. We are almost done. We only need to fill
out the last 2 fields. In that fields you need to enter your new password. It is
important that the password in both fields is the same!. The last thing you will
need to do is to click the blue button that says "Зарегистрироваться".

..  image:: ../pictures/createAccount-step3.png


Congratulations! You have created a new account!
