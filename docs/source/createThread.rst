How to create a new thread?
===========================

******
Step 1
******

Welcome to this guide. In this guide I am going to show you how to create a thread
on the forum. It is important that you are logged into an account. If you are then
you need to head to the forum itself. You can do this by pressing the link on the
menubar. This will redirect you to the main forum page.

..  image:: ../pictures/createThread-step1.png

******
Step 2
******

Now you are on the main forum page. In order to create a new thread you need to
click the blue button with the text: "Новая тема". After you click it you will be
on another page with a thread creation form.

..  image:: ../pictures/createThread-step2.png

******
Step 3
******

The only thing left to do is to fill you the form. There are only 2 fields. In the
first field you need to enter the title of your thread. In the next field you can
write whatever you want. Here is an example:

..  image:: ../pictures/createThread-step3.png

The only thing left to do is to publish your thread. You can do it by pressing the
blue button under the form that is saying "Создать".

Congratulations! Now you know how to create a new thread. You are becoming an
experienced user!!!