Developer documentation
=======================
.. toctree::
   :maxdepth: 2
   :caption: Django Apps:

   calculator.rst
   forum.rst
   users.rst
