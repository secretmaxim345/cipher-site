.. cipher_site documentation master file, created by
   sphinx-quickstart on Tue May  4 16:57:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cipher_site's documentation!
=======================================

.. toctree::
   :maxdepth: 3
   :caption: Documentation

   userDocumentation.rst
   developer.rst