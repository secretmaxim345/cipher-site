How to log into an account?
===========================

******
Step 1
******

Welcome to this guide. In this guide I am going to show you how to sign into an
account on our website. It's pretty easy to do. Just follow my steps.
To sign into an account, it isn't important on which page you are. On the top
right corner you can see a blue button that says "Войти". If you see a blue button
saying "Выйти" instead it means you are already logged in. Click on the blue button
saying "Войти". It will redirect you to a login page.

..  image:: ../pictures/createAccount-step1.png

******
Step 2
******

On this page you will see a form. You need to fill it out with your account
information. There are 2 fields you need to fill out. In the first field you need
to write your username and in the second one you need to enter your password here
is an example.

..  image:: ../pictures/Login-step2.png

Now when you filled out the form you need to click the "Войти" button. This will
sign you into the account. And that's how you do it!
