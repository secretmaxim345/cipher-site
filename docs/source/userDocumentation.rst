User documentation
==================

.. toctree::
   :maxdepth: 2
   :caption: Django Apps:

   createAccount.rst
   login.rst
   createThread.rst
   calculatorUsage.rst