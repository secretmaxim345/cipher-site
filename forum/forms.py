from django import forms
from forum.models import Thread, Comment
from ckeditor.fields import RichTextField


class CreateThreadForm(forms.ModelForm):
    """
    This form is used when creating a new forum thread
    """
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'thread_title', 'placeholder': 'Заголовок'}))

    class Meta:
        model = Thread
        fields = (
            'title',
            'body',
        )


class NewComment(forms.ModelForm):
    """
    This form is used when creating a new comment on a forum thread
    """

    class Meta:
        model = Comment
        fields = (
            'body',
        )
