import ast
from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
from users.models import Users


class Thread(models.Model):
    """
    This is a model of a forum thread
    """

    author = models.IntegerField()
    title = models.CharField(max_length=100)
    body = RichTextField(blank=True, null=True)
    comment_ids = models.CharField(default="[]", max_length=10000)
    creation_date = models.DateTimeField(default=timezone.now)

    def get_comments(self) -> list:
        """
        gets a list of comment ids

        :return: list of comment ids
        """
        return ast.literal_eval(self.comment_ids)

    def get_comment_length(self):
        """
        get the number of comments on a thread

        :return: number of comments
        """
        return len(self.get_comments())

    def get_author(self):
        """
        gets the author of the thread

        :return: author of the thread
        """
        return Users.objects.get(id=self.author)

    def add_comment(self, comment):
        """
        adds a comment to the data base

        :param comment: comment id
        :type comment: int or str
        :return: Nothing
        :rtype: None
        """
        comments = self.get_comments()
        comments.append(int(comment))
        self.comment_ids = str(comments)


class Comment(models.Model):
    """
    This is a model of a forum thread comment
    """
    author = models.IntegerField()
    body = RichTextField(blank=True, null=True)
    creation_date = models.DateTimeField(default=timezone.now)

    def get_author(self):
        """
        This method gets the author of the comment.

        :return: the author of the comment
        """
        return Users.objects.get(id=self.author)
