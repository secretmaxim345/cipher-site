from django.contrib import admin
from django.urls import path

from forum import views

app_name = 'forum'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('<str:room_name>/', views.thread),
]
