from django.shortcuts import render, redirect
from django.views.generic import ListView
from forum.models import Thread, Comment
from forum.forms import CreateThreadForm, NewComment
from users.models import Users


def create_thread(request):
    """
    This function is a view function. It creates a new thread.

    :param request: an object with request details
    :type request: :class:`django.http.HttpRequest`
    :return: server response object with HTML code
    :rtype: :class:`django.http.HttpResponse`
    """
    if request.method == 'POST':
        form = CreateThreadForm(request.POST)

        if form.is_valid():
            new_thread = Thread(author=request.user.id,
                                title=form.cleaned_data['title'],
                                body=form.cleaned_data['body'])

            new_thread.save()
            return redirect('thread/' + str(new_thread.id))
    else:
        form = CreateThreadForm

    context = {
        'form': form,
        'page_name': 'Создание темы'
    }
    return render(request, 'forum/create_thread.html', context)


def thread(request, room_name):
    """

    :param request: an object with request details
    :type request: :class:`django.http.HttpRequest`
    :param room_name: the id of the room
    :type room_name: str
    :return: server response object with HTML code
    :rtype: :class:`django.http.HttpResponse`
    """
    print(type(room_name))
    thread_data = Thread.objects.get(id=room_name)
    author = Users.objects.get(id=thread_data.author)

    comments = [Comment.objects.get(id=int(comment_id))
                for comment_id in thread_data.get_comments()]

    if request.method == 'POST':
        form = NewComment(request.POST)
        print('success')
        if form.is_valid():
            new_comment = Comment(author=request.user.id,
                                  body=form.cleaned_data['body'])

            new_comment.save()
            comments.append(new_comment)
            thread_data.add_comment(new_comment.id)
            thread_data.save()

        return redirect(request.get_full_path())

    form = NewComment()

    context = {
        'data': thread_data,
        'comments': comments,
        'form': form,
        'author': author,
        'page_name': 'Тема: {}'.format(thread_data.title)
    }
    return render(request, 'forum/thread.html', context)


class ListThreads(ListView):
    """
    This view class displays a list of forum threads.
    """
    model = Thread
    extra_context = {
        'page_name': 'Форум'
    }
    template_name = 'forum/forum.html'
