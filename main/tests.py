from django.test import TestCase, Client
from django.urls import reverse


class MainTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.response = self.client.get(reverse('home'))

    def test_home_response(self):
        self.assertEqual(self.response.status_code, 200)
