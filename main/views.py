from django.shortcuts import render


def home(request):
    context = {
        'page_name': 'Главная',
    }
    return render(request, 'pages/home.html', context)


def lessons(request):
    context = {
        'page_name': 'Уроки',
    }
    return render(request, 'pages/lessons.html', context)
