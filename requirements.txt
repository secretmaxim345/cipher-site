Django~=3.1.7
-e ./ckeditor
django-debug-toolbar~=3.2.1
channels~=3.0.3
Pillow~=8.2.0
coverage~=5.5
pylint==2.7.4
pylint-django==2.4.4
anybadge==1.7.0
heroku~=0.1.4
django-heroku~=0.3.1
gunicorn~=20.1.0
python-decouple~=3.4
django-ckeditor~=6.0.0