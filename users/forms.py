from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.functional import lazy

from .models import Users


class RegistrationForm(UserCreationForm):
    """
    This is a registration form.
    It is used to create a new user.
    The form contains 6 fields
    -username
    -email
    -first name
    -last name
    -password
    -confirm password
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].label = 'Пароль'
        self.fields['password2'].label = 'Подтверждение пароля'

    class Meta:
        model = Users
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2'
        ]
        labels = {
            'username': 'Никнейм',
            'email': 'Почта',
            'first_name': 'Имя',
            'last_name': 'Фамилия',

        }


class EditProfileForm(UserChangeForm):
    """
    This is a form that is used to edit users profile.
    It contains 4 fields that you can edit
    -username
    -first name
    -last name
    -profile picture
    """
    password = None

    class Meta:
        model = Users
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'profile_picture',
        ]
        labels = {
            'username': 'Никнейм',
            'email': 'Почта',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'profile_picture': 'Фото профиля',
        }
