from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone


class UserManager(BaseUserManager):
    """
    This class is a user manager. This class can create new users.
    """
    def create_user(self, username, email, first_name, last_name, password=None):
        """
        This method is validating the data and creating a new user

        :param username: username of the new user
        :param email: email of the new user
        :param first_name: first name of the new user
        :param last_name: last name of the new user
        :param password: password of the new user
        :return: a new user
        """
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have an username")
        if not first_name or not last_name:
            raise ValueError("Users must have a full name")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(user=self.db)
        return user


class Users(AbstractBaseUser):
    """
    This is a user model. It was inherited from :class:`django.contrib.auth.models.AbstractBaseUser`
    """
    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField(max_length=60, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    creationDate = models.DateTimeField(default=timezone.now)
    profile_picture = models.ImageField(default='profile_pics/default.jpg',
                                        upload_to='profile_pics')
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    objects = UserManager()

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = [
        'email',
        'first_name',
        'last_name',
    ]

    def has_module_perms(self, app_label):
        """
        This class checks if the user has permission to do something

        :param app_label: the lapel of the app to get access to
        :return: if the user has permission or not
        """
        return self.is_admin

    def has_perm(self, app_label):
        """
        This class checks if the user has permission to do something

        :param app_label: the lapel of the app to get access to
        :return: if the user has permission or not
        """
        return self.is_admin
