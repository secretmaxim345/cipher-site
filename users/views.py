from django.contrib.auth import login, authenticate
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic.edit import UpdateView

from .forms import RegistrationForm, EditProfileForm
from .models import Users


class Registration(CreateView):
    """
    This is a a view class which is inherited from :class:`django.views.generic.CreateView`.
    Its purpose is to send a registration html to the client.

    :param model: the model that is going to be created
    :param form_class: the form that is going to be used
    :param template_name: the html of the page
    :param success_url: the url that the user is going to be redirected after registration
    """
    model = Users
    form_class = RegistrationForm
    template_name = 'registration/registration.html'
    success_url = reverse_lazy('edit_profile')

    extra_context = {
        'page_name': 'Регистрация',
    }

    def form_valid(self, form):
        """
        this method checks if the form is valid and signs in the user.

        :param form: form that is going to be validated
        :type form: :class:`django.forms.form`
        :return: true if form is valid. if not, false
        :rtype:bool
        """
        valid = super().form_valid(form)
        username, password = form.cleaned_data.get('username'), form.cleaned_data.get('password1')
        new_user = authenticate(username=username, password=password)
        login(self.request, new_user)
        return valid


class EditProfile(UpdateView):
    """
    This is a a view class which is inherited from :class:`django.views.generic.CreateView`.
    Its purpose is to send a edit profile html to the client.

    :param model: the model that is going to be created
    :param form_class: the form that is going to be used
    :param template_name: the html of the page
    :param success_url: the url that the user is going to be redirected after the account is edited
    """
    model = Users
    form_class = EditProfileForm
    template_name = 'registration/edit_profile.html'
    success_url = reverse_lazy('edit_profile')

    extra_context = {
        'page_name': 'Ваш профиль',
        'page_heading': 'Ваш профиль'
    }

    def get_object(self, **kwargs):
        return self.request.user
